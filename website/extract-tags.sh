#!/bin/bash
grep -o '{{[a-z _]*}}' ${1} | sed -e 's/{{//g' | sed -e 's/}}//g' | awk '{print "\""$1"\":\""$1"\","}'
