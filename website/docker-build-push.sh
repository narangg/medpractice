#!/bin/bash

tag=dev
isPush="noPush"

isPush=$1

docker build -t narangg/medpractice:qa .
if [ "$isPush" = "push" ]; then
	docker push narangg/medpractice:qa
fi

