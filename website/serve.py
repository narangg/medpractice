#!/usr/bin/python3
# serve.py

from flask import Flask
from flask import render_template, send_from_directory
import json
import collections
import copy
import subprocess
import datetime as dt


# creates a Flask application, named app
app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    tm = read_text_map()
    print (type(tm))
    if path:
        #return render_template(path, message=message)
        if 'html' in path:
            return render_template(path, **tm)
        else:
            return send_from_directory('templates', path)
    else:
        #return render_template('index.html', message=message)
        return render_template('index.html', **tm)

class TransformedDict(collections.MutableMapping):
    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def __getitem__(self, key):
        value = self.store[self.__keytransform__(key)]
        if value is None:
            value = '<<{}>>'.format(key)
        
        return value

    def __setitem__(self, key, value):
        self.store[self.__keytransform__(key)] = value

    def __delitem__(self, key):
        del self.store[self.__keytransform__(key)]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __keytransform__(self, key):
        return key

_mapping = None
def read_text_map_old():
    global _mapping
    if _mapping is not None:
        print ("Found mapping")
        print (type(_mapping))
        return _mapping
    else:
        print ("Did not find mapping")
        _mapping = TransformedDict()
        

    with open('text-mapping.json') as json_file:
        x = json.load(json_file)
        for item in x:
            #print (item, x.get(item))
            _mapping[item] = x.get(item)
        #print (_mapping)

    return _mapping

#def read_text_map_from_sheets():
last_read = dt.datetime.now()
def read_text_map():
    global _mapping
    global last_read
    if _mapping is not None and dt.datetime.now() - last_read < dt.timedelta(seconds=10) :
        print ("Found mapping. Returning Cached memory")
        print (type(_mapping))
        return _mapping
    else:
        print ("Did not find fresh enough mapping. Recreating a new one.")
        last_read = dt.datetime.now()
        _mapping = TransformedDict()

    url = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQENE_QJCOy6uolferaOtvgFtaRkNPmrY94auvmPmqc_V9ow8KOw138D81AAUtweRwc_oKeArlIc681/pub?gid=0&single=true&output=tsv'
    urlDropbox = 'https://www.dropbox.com/s/hxfhob54404dchn/tags.tsv?dl=0'
    cmd = "curl -L -s \'{}\' --output  tags.tsv".format(urlDropbox)
    print ("executing " + cmd)

    subprocess.call(cmd, shell=True)

    with open('tags.tsv') as f:
        for line in f:
            tag, value = line.split('\t')
            #print (tag, value)
            _mapping[tag] = value

    #print (_mapping)
    return _mapping

# run the application
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
